stages:
  - test
  - build
  - pre-deploy
  - deploy

image: docker:latest
services:
  - docker:dind

unit-test-job: # This job runs in the test stage.
  stage: test # It only starts when the job in the build stage completes successfully.
  script:
    - echo "Running unit tests...This will take about 60 seconds."
    - sleep 60
    - echo "Code coverage is 90%"

lint-test-job: # This job also runs in the test stage.
  stage: test # It can run at the same time as unit-test-job (in parallel).
  script:
    - echo "Linting code... This will take about 10 seconds."
    - sleep 10
    - echo "No lint issues found."

before_script:
  # Login to Google Container Registry
  - base64 -d ${GCP_SA_KEY_DE} | docker login -u _json_key --password-stdin https://asia.gcr.io
  # Login to Gitlab container Registry
  - echo $CI_JOB_TOKEN | docker login -u gitlab-ci-token $CI_REGISTRY --password-stdin

build:docker:
  stage: build
  script:
    # Build and tag image for both GCR and Gitlab registries
    - docker build -t asia.gcr.io/$PROJECT_ID/$CI_PROJECT_NAMESPACE/$CI_PROJECT_NAME:$CI_COMMIT_SHORT_SHA -t $CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA .
    # Push image to GCR
    - docker push asia.gcr.io/$PROJECT_ID/$CI_PROJECT_NAMESPACE/$CI_PROJECT_NAME:$CI_COMMIT_SHORT_SHA
    # Push image to Gitlab registry
    - docker push $CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA
  only:
    refs:
      - main
      - /^releases/(\d+.\d+.\d+)(-.*)?/

# Init deploy
pre:platform-dev:
  stage: pre-deploy
  image: google/cloud-sdk
  tags:
    - docker
  script:
    - echo "Instaling heml chart:"
    - curl https://baltocdn.com/helm/signing.asc | apt-key add -
    - apt-get install apt-transport-https --yes
    - echo "deb https://baltocdn.com/helm/stable/debian/ all main" | tee /etc/apt/sources.list.d/helm-stable-debian.list
    - apt-get update
    - apt-get install helm
    - helm repo add bitnami https://charts.bitnami.com/bitnami

    - echo "Authenticate by GSC:"
    - gcloud auth activate-service-account $GCP_ACCOUNT --key-file=$GCP_SA_KEY --project=$PROJECT_ID
    - gcloud container clusters get-credentials guestbook --zone=asia-east2-a

    # Kubectl
    - echo "Create namespace"
    - kubectl apply -f deploy/namespace/development.yaml
    - echo "Switch to development namespace:"
    - kubectl config set-context --current --namespace development

    # Apply yml files
    - echo "Update yaml file:"
    - helm install redis bitnami/redis --set auth.enabled=false
    - kubectl apply -f deploy/deployment.yaml
    - kubectl apply -f deploy/service.yaml
    # Echo current service and it's IP
    - kubectl get service guestbook-service-load-balancer
    - kubectl apply -f deploy/hpa.yaml

  only:
    refs:
      - main
    changes:
      - deploy/**/*

pre:platform-staging:
  stage: pre-deploy
  image: google/cloud-sdk
  tags:
    - docker
  script:
    - echo "Instaling heml chart:"
    - curl https://baltocdn.com/helm/signing.asc | apt-key add -
    - apt-get install apt-transport-https --yes
    - echo "deb https://baltocdn.com/helm/stable/debian/ all main" | tee /etc/apt/sources.list.d/helm-stable-debian.list
    - apt-get update
    - apt-get install helm
    - helm repo add bitnami https://charts.bitnami.com/bitnami

    - echo "Authenticate by GSC:"
    - gcloud auth activate-service-account $GCP_ACCOUNT --key-file=$GCP_SA_KEY --project=$PROJECT_ID
    - gcloud container clusters get-credentials guestbook --zone=asia-east2-a

    # Kubectl
    - echo "Create namespace"
    - kubectl apply -f deploy/namespace/staging.yaml
    - echo "Switch to staging namespace:"
    - kubectl config set-context --current --namespace staging

    # Apply yml files
    - echo "Update yaml file:"
    - helm install redis bitnami/redis --set auth.enabled=false
    - kubectl apply -f deploy/deployment.yaml
    - kubectl apply -f deploy/service.yaml
    # Echo current service and it's IP
    - kubectl get service guestbook-service-load-balancer
    - kubectl apply -f deploy/hpa.yaml

  only:
    refs:
      - /^releases/(\d+.\d+.\d+)(-.*)?/
    changes:
      - deploy/**/*

deploy:dev:
  stage: deploy
  image: google/cloud-sdk
  environment: Development
  script:
    - echo "Deploying application Development"
    - gcloud auth activate-service-account $GCP_ACCOUNT --key-file=$GCP_SA_KEY --project=$PROJECT_ID
    - gcloud config set project $PROJECT_ID
    - gcloud container clusters get-credentials guestbook --zone=asia-east2-a

    - echo "Switch to gues-book-dev namespace:"
    - kubectl config set-context --current --namespace development
    - kubectl set image deployment/guestbook-app guestbook-web=asia.gcr.io/$PROJECT_ID/$CI_PROJECT_NAMESPACE/$CI_PROJECT_NAME:${CI_COMMIT_SHORT_SHA} --record
    - echo "Application successfully deployed."

  only:
    refs:
      - main

deploy:stg: # This job runs in the deploy stage.
  stage: deploy # It only runs when *both* jobs in the test stage complete successfully.
  image: google/cloud-sdk
  environment: Staging
  when: manual
  script:
    - echo "Deploying application to Staging.."
    - gcloud auth activate-service-account $GCP_ACCOUNT --key-file=$GCP_SA_KEY --project=$PROJECT_ID
    - gcloud config set project $PROJECT_ID
    - gcloud container clusters get-credentials guestbook --zone=asia-east2-a
    - echo "Switch to staging namespace:"
    - kubectl config set-context --current --namespace staging
    - kubectl set image deployment/guestbook-app guestbook-web=asia.gcr.io/$PROJECT_ID/$CI_PROJECT_NAMESPACE/$CI_PROJECT_NAME:${CI_COMMIT_SHORT_SHA} --record
    - echo "Application successfully deployed."
  only:
    refs:
      - /^releases/(\d+.\d+.\d+)(-.*)?/
