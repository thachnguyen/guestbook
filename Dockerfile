FROM python:3.7
WORKDIR /app

RUN apt-get update && \
  apt-get install -y python-dev python-pip && \
  pip install redis flask

ADD . .

CMD [ "python", "src/app.py" ]
EXPOSE 80