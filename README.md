## Guestbook project

[![pipeline status](https://gitlab.com/thachnguyen/guestbook/badges/main/pipeline.svg)](https://gitlab.com/thachnguyen/guestbook/-/commits/main)
[![coverage report](https://gitlab.com/thachnguyen/guestbook/badges/main/coverage.svg)](https://gitlab.com/thachnguyen/guestbook/-/commits/main)

### Poppose

- Help me learning some skill :
  - Linux: bash script
  - Build dockerfile to Dev env and Deployment
  - Using Google Cloud: IAM & Admin, GKE(Kubernetes Engine), GCR(Google Container Registry), Heml Chart, Redis
  - Defined deploy script
  - Integrate gitlab(Gitlab Runner, Gitlab Pipeline) CI/CD and deploy to Dev, Staging and Production env
  - Send Gitlab notification to Telegram via webhook
  
- More futures:
  - Build Logging & Monitoring(Grafana, Prometheus, Loki)

### Docker

#### Setup

- Build docker containers:

```
docker-compose build
docker-compose up
docker-compose restart app
```

#### Start Application

```
docker-compose up
```

---

#### Product (will close if out of billing)

- Development: http://35.220.228.125/

- Staging: http://35.220.237.132/
